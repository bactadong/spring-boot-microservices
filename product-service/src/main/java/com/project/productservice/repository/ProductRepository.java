package com.project.productservice.repository;

import com.project.productservice.model.Employee;
import com.project.productservice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {




}
